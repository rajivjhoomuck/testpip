//
//  FullScreenAnimator.swift
//  TestPiP
//
//  Created by Rajiv Jhoomuck on 09/04/2018.
//  Copyright © 2018 Rajiv Jhoomuck. All rights reserved.
//

import UIKit

protocol FullScreenPresentable {
  var containingView: UIView? { get }
  var containingViewController: UIViewController? { get }
  var windowCoordinateSpaceBasedContainingViewFrame: CGRect { get }
}

class FullScreenAnimator<ViewController: UIViewController>: NSObject, UIViewControllerAnimatedTransitioning where ViewController: FullScreenPresentable {

  private let isPresenting: Bool
  private let transitionDuration: TimeInterval = 0.3


  init(forPresenting presenting: Bool) {
    self.isPresenting = presenting

    super.init()
  }

  func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
    return transitionDuration
  }

  func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

    let fullScreenViewControllerKey: UITransitionContextViewControllerKey = isPresenting ? .to : .from
    let fullScreenViewKey: UITransitionContextViewKey = isPresenting ? .to : .from

    guard
      let fullScreenViewController = transitionContext.viewController(forKey: fullScreenViewControllerKey) as? ViewController,
      let fullScreenView = transitionContext.view(forKey: fullScreenViewKey)
      else {
        return
    }

    let containerView = transitionContext.containerView

    if isPresenting {
      fullScreenView.removeFromSuperview()
      fullScreenView.frame = fullScreenViewController.windowCoordinateSpaceBasedContainingViewFrame
      containerView.addSubview(fullScreenView)

      UIView.animate(withDuration: transitionDuration, animations: {
        fullScreenView.frame = transitionContext.finalFrame(for: fullScreenViewController)
        fullScreenView.layoutIfNeeded()
      }, completion: { _ in
        let success = !transitionContext.transitionWasCancelled

        if !success {
          // Do more than that, especially when considering pinch to zoom.
          fullScreenView.removeFromSuperview()
        }

        transitionContext.completeTransition(success)
      })

    } else {
      containerView.addSubview(fullScreenView)

      var originFrame = fullScreenViewController.windowCoordinateSpaceBasedContainingViewFrame

      UIView.animate(withDuration: transitionDuration, animations: {
        fullScreenView.frame = originFrame
        fullScreenView.layoutIfNeeded()
      }, completion: { _ in
        let success = !transitionContext.transitionWasCancelled

        defer {
          transitionContext.completeTransition(success)
        }

        fullScreenView.removeFromSuperview()

        guard
          let containingViewController = fullScreenViewController.containingViewController,
          let containingView = fullScreenViewController.containingView
          else {
            return
        }

        DispatchQueue.main.async {
          containingViewController.addChildViewController(fullScreenViewController)

          originFrame.origin = .zero
          fullScreenView.frame = originFrame
          containingView.addSubview(fullScreenView)
          NSLayoutConstraint.activate([
            fullScreenView.leadingAnchor.constraint(equalTo: containingView.leadingAnchor),
            fullScreenView.trailingAnchor.constraint(equalTo: containingView.trailingAnchor),
            fullScreenView.topAnchor.constraint(equalTo: containingView.topAnchor),
            fullScreenView.bottomAnchor.constraint(equalTo: containingView.bottomAnchor)
            ])

          fullScreenViewController.didMove(toParentViewController: containingViewController)
        }
      })
    }
  }
}
