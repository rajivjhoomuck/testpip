//
//  PlayerView.swift
//  TestPiP
//
//  Created by Rajiv Jhoomuck on 09/04/2018.
//  Copyright © 2018 Rajiv Jhoomuck. All rights reserved.
//

import UIKit
import AVKit

final class PlayerView: UIControl {

  /// The `AVPlayer` will be set to control the layer of this view.
  var player: AVPlayer? {
    get {
      return playerLayer.player
    }
    set {
      playerLayer.player = newValue
    }
  }

  /// The CALayer subclass backing this view.
  var playerLayer: AVPlayerLayer {
    return layer as! AVPlayerLayer
  }

  /// Inform UIKit about the class of the CALayer that  needs to be created to back this view.
  override static var layerClass: AnyClass {
    return AVPlayerLayer.self
  }

  override init(frame: CGRect) {
    super.init(frame: frame)
    configure()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    configure()
  }

  func configure() {
    backgroundColor = .black
    clearsContextBeforeDrawing = false

    // Add a target to the playerView to toggle the overlay visibility
    //addTarget(nil, action: #selector(PlayerViewController.userDidTapOnPlayerView), for: .touchUpInside)
  }
}
