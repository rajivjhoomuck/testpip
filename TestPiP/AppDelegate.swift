//
//  AppDelegate.swift
//  TestPiP
//
//  Created by Rajiv Jhoomuck on 09/04/2018.
//  Copyright © 2018 Rajiv Jhoomuck. All rights reserved.
//

import UIKit
import AVKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

  var window: UIWindow?


  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    let audioSession = AVAudioSession.sharedInstance()
    do {
      try audioSession.setCategory(AVAudioSessionCategoryPlayback)
    } catch {
      print("Failed to set the audio session category to Playback. There will be no audio if device is in Silent mode")
    }

    return true
  }
}
