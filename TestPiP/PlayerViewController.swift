//
//  PlayerViewController.swift
//  TestPiP
//
//  Created by Rajiv Jhoomuck on 09/04/2018.
//  Copyright © 2018 Rajiv Jhoomuck. All rights reserved.
//

import UIKit
import AVKit

class PlayerViewController: UIViewController {
  /// The view that contains the player view controller's view. This is set in `didMove(toParentViewController:)`
  weak private(set) var containingView: UIView?

  @IBOutlet weak var pipToggle: UIButton!
  var pictureInPictureController: AVPictureInPictureController?

  // The view controller containing the player view controller. This is set in `didMove(toParentViewController:)`
  weak private(set) var containingViewController: UIViewController?

  @IBOutlet weak var fullScreenToggle: UIButton! {
    didSet {
      fullScreenToggle.addTarget(nil, action: #selector(PlayerViewController.toggleFullScreen), for: .touchUpInside)
    }
  }

  private var isPlayerExpanded: Bool = false


  var player: AVPlayer? {
    didSet { playerView.player = player }
  }
  private var playerItemStatusObserver: NSKeyValueObservation?

  private let playerView: PlayerView = {
    let playerView = PlayerView()
    playerView.clipsToBounds = true
    return playerView
  }()

  public override func didMove(toParentViewController parent: UIViewController?) {
    super.didMove(toParentViewController: parent)

    if parent != nil {
      containingView = view.superview
      containingViewController = parent
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    
    view.clipsToBounds = true
    view.insertSubview(playerView, at: 0)
    playerView.translatesAutoresizingMaskIntoConstraints = false
    NSLayoutConstraint.activate(
      [
        playerView.topAnchor.constraint(equalTo: view.topAnchor),
        playerView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
        playerView.leftAnchor.constraint(equalTo: view.leftAnchor),
        playerView.rightAnchor.constraint(equalTo: view.rightAnchor)
      ]
    )

    modalPresentationStyle = .custom
    transitioningDelegate = self

    let url = URL(string: "https://bitdash-a.akamaihd.net/content/sintel/hls/playlist.m3u8")!
    player = AVPlayer(url: url)
    player?.play()

    if AVPictureInPictureController.isPictureInPictureSupported() {
      guard let playerItem = player?.currentItem else { return }
      playerItemStatusObserver = playerItem.observe(\AVPlayerItem.status, options: [.new, .initial]) { [weak self] (item, _) in
        guard let strongSelf = self else { return }

        // Display an error if status becomes Failed

        if item.status == .failed {
          print("Failed ⚠️")
        } else if item.status == .readyToPlay {
          if strongSelf.pictureInPictureController == nil {
            strongSelf.setUpPictureInPicture()
          }
        }
      }
      setUpPictureInPicture()
    } else {
      pipToggle.isHidden = true
    }
  }
}

// MARK: Picture in Picture
extension PlayerViewController {
  func setUpPictureInPicture() {
    let startImage = AVPictureInPictureController.pictureInPictureButtonStartImage(compatibleWith: nil)

    pictureInPictureController = AVPictureInPictureController(playerLayer: playerView.playerLayer)
    pictureInPictureController?.delegate = self
    pipToggle.isHidden = false
    pipToggle.setImage(startImage, for: .normal)
    pipToggle.addTarget(self, action: #selector(togglePictureInPicture(_:)), for: .touchUpInside)
  }

  @objc func togglePictureInPicture(_ sender: UIButton) {
    guard let pictureInPictureController = pictureInPictureController else { return }

    if pictureInPictureController.isPictureInPictureActive {
      pictureInPictureController.stopPictureInPicture()
    } else {
      pictureInPictureController.startPictureInPicture()
      print("Starting PiP")
    }
  }
}

// MARK: - FullScreenPresentable delegate
extension PlayerViewController: FullScreenPresentable {
  // The containing view's frame in the window coordinate space.
  var windowCoordinateSpaceBasedContainingViewFrame: CGRect {
    return containingView.map { $0.convert($0.bounds, to: nil) } ?? .zero
  }
}

// MARK: Picture in Picture
extension PlayerViewController: AVPictureInPictureControllerDelegate {
  public func picture(_ pictureInPictureController: AVPictureInPictureController, restoreUserInterfaceForPictureInPictureStopWithCompletionHandler completionHandler: @escaping (Bool) -> Void) {
    completionHandler(false)
  }

  public func pictureInPictureControllerDidStartPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
    let pipImage = AVPictureInPictureController.pictureInPictureButtonStopImage(compatibleWith: nil)
    pipToggle.setImage(pipImage, for: .normal)
  }

  public func pictureInPictureControllerWillStopPictureInPicture(_ pictureInPictureController: AVPictureInPictureController) {
    let pipImage = AVPictureInPictureController.pictureInPictureButtonStartImage(compatibleWith: nil)
    pipToggle.setImage(pipImage, for: .normal)
  }

  public func pictureInPictureControllerFailedToStartPictureInPicture(pictureInPictureController: AVPictureInPictureController, withError error: NSError) {
    print("💩💩💩💩💩")
  }
}

// MARK: Fullscreen
extension PlayerViewController {
  @objc func toggleFullScreen() {
    if presentingViewController == nil {
      expandPlayer()
    } else {
      collapsePlayer()
    }
  }

  public func expandPlayer() {
    // check if player is already expanded
    if isPlayerExpanded {
      return
    }

    isPlayerExpanded = true


    willMove(toParentViewController: nil)
    removeFromParentViewController()
    // !!!: We are not removing self.view from the superview yet. That will be done in the animator.
    containingViewController?.present(self, animated: true) {}
  }

  /// Dismisses the `PlayerViewController` that has been presented in fullscreen. This method does nothing if the `playerViewController` is not in fullscreen.
  public func collapsePlayer() {
    // check if player is already collapsed
    if !isPlayerExpanded {
      return
    }

    isPlayerExpanded = false
    dismiss(animated: true, completion: nil)
  }
}

// MARK: - UIViewControllerTransitioningDelegate
extension PlayerViewController: UIViewControllerTransitioningDelegate {
  public func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return FullScreenAnimator<PlayerViewController>(forPresenting: true)
  }

  public  func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {
    return FullScreenAnimator<PlayerViewController>(forPresenting: false)
  }
}
