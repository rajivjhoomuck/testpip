# A Sample app that demonstrates a PiP bug on iPad Pros (mid 2018 generation) but possibly on other iPad models as well.


So basically, I have this video app where I have customized the fullscreen presentation / transition. After adding PiP support, I saw weird behavior on an iPad Pro. Activating PiP on my player view controller, minimizes the video layer to the corners of the player view controller rather the corners of the screen. This only happens once I make the first transition to fullscreen. I contacted DTS regarding this issue and got a reply from Apple engineers who also succeeded in reproducing the bug.

Steps to Reproduce:
Normal behavior:
1. Run test app (attached) on an iPad Pro.
2. When stream is loaded, tap PiP button to transition to PiP playback.

Buggy behavior:
1. Run test app (attached) on an iPad Pro.
2. When stream is loaded, tap fullscreen button to go fullscreen, then exit fullscreen.
3. Tap PiP to transition to PiP playback.

Expected Results:
PiP starts with video layer minimized to one of the corners of the **screen**

Actual Results:
Normal behavior:
=> PiP starts with video layer minimized to one of the corners of the **screen**

Buggy behavior:
=> PiP starts with video layer minimized to one of the corners of the **embedded view controller**

Version/Build:
Tested on iOS 11.x

Configuration:
iPad Pro running iOS 11.3
